// Advance Query Operators
	// We want more flexible querying of data within MongoDB.

// [Section] Comparison query operators

	// $gt/$gte operator
	/*
		- Allows us to find documents that hav edield number values greater than or equal to specified vallue.
		- Syntax:
			db.collectionName.find({field:{$gt/$gte: value}});
	*/

	db.users.find(
		{
			age:{
				$gt: 65
			}
		}
	);

		db.users.find(
		{
			age:{
				$gte: 65
			}
		}
	);

	// $lt/$lte Operator
	/*
		allows us to find documents that have the field numbervalues less than or equal ro a specified value 
		- Syntax:
			- db.collectionName.find({field:{$lt/$lte: value}});

	*/

	db.users.find(
		{
			age:{
				$lt: 65
			}
		}
	);

	db.users.find(
		{
			age:{
				$lte: 65
			}
		}
	);

	// $ne Operators 
	/*
		-Allows us to find socuments field number values not wqual to a specified value.
		- Syntax:
			- db.collectionName.find({field:{$ne: value}});
	*/

	db.users.find(
		{
			age:{
				$ne: 82
			}
		}
	);

	// $in Operator
	/*
		- allows us to find documents with specific match criteria one with one field using differnt values.
		- Syntax:
			- db.collectionName.find({field:{$in:[valueA, valueB]}});
	*/

	db.users.find(
		{
			lastName:{
				$in:["Hawking", "Doe"]
			}
		}
	);

	db.users.find(
		{
			courses:
			{
				$in:["HTML", "React"]
			}
		}
	);

// [SECTION] Logical Query Operators

// $or operator
/*
	- Allow us to find documents that match a single criteria form multiple provided search criteria.
	- Syntax:
		- db.collectionName.find({$or: [{fieldA:fieldA}, {fieldB:fieldB}]});
*/
	
	// Multiple field value pairs
	db.users.find(
		{
			$or:
			[
				{firstName: "Neil"},
				{age: 25 }
			]
			
		}
	);

	db.users.find(
		{
			$or:
			[
				{firstName: "Neil"},
				{age: {$gt: 25}}
			]
			
		}
	);

// $and Operator
/*
	- Allows us to find documents matching multiple criteria in a single field.
	- Syntax:
		- db.collectionName.find({$and: [{fieldA:valueA}, {fieldB:valueB}]});
*/

db.users.find(
		{
			$and:
			[
				{age: {$ne: 82}},
				{age: {$ne: 76}}
			]
			
		}
	);

db.users.find(
		{
			$and:
			[
				{age: {$ne: 82}},
				{department: "Operations"}
			]
			
		}
	);

// [SECTIONS] Field Projection
// To help eith the reaadability of the values returned, we can include/excludes fields from the response.

// Inclusion
/*
	- Allow us to include/add specific fields only when retrieving a document.
	- 
	- Syntax:
		- db.collectionName.find({criteria, {field: 1});
*/

db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	);

// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents.
	- The value provided is 0 to denote that fields is being excluded.
	- Syntax:
		- db.collectionName.find({criteria, {field: 0});

*/

db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 1
	}
);

// Supressing the ID field
/*
	- when using field projection, field inclusion may not be used at the same time.
	- Excluding the "_id" field id the only exeption to this rule.
	- Syntax:
		- db.collectionName.find({criteria, {_id: 0});

*/

db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1,
			_id: 0
		}
	);

// [SECTION] Evaluation Query Operators
// $regex operator
/*
	- allows us to find documents that match specefic pattern using regular expressions.
	- Syntax:
		- db.collectionName.find({field: $regex: "pattern", $options: $optionValue});
*/

	// case sensetive query
	db.users.find(
		{firstName: 
			{
				$regex: 'N'
			}
		}
	);

	// case insensitivity
	db.users.find(
		{firstName: 
			{
				$regex: 'N',
				$options: '$i'
			}
		}
	);